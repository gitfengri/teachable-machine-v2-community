(function () {
  /*

  Copyright The Closure Library Authors.
  SPDX-License-Identifier: Apache-2.0
 */
  'use strict';
  var g = this || self;
  function h(a) {
    if (a && a != g) return m(a.document);
    null === p && (p = m(g.document));
    return p;
  }
  var q = /^[\w+/_-]+[=]{0,2}$/,
    p = null;
  function m(a) {
    return (a = a.querySelector && a.querySelector('script[nonce]')) &&
      (a = a.nonce || a.getAttribute('nonce')) &&
      q.test(a)
      ? a
      : '';
  }
  function r(a) {
    var b = ['document', 'location', 'href'];
    a = a || g;
    for (var d = 0; d < b.length; d++)
      if (((a = a[b[d]]), null == a)) return null;
    return a;
  }
  function t(a) {
    var b = typeof a;
    return ('object' == b && null != a) || 'function' == b;
  }
  function u(a) {
    return a;
  }
  var v;
  var w;
  function x(a, b) {
    this.h = (a === y && b) || '';
    this.g = z;
  }
  var z = {},
    y = {};
  function A(a, b) {
    this.g = b === B ? a : '';
  }
  function C() {
    var a = new x(
      y,
      'https://www.gstatic.com/support/help/staging/main_frame/help_panel_staging_binary.js'
    );
    return D(
      a instanceof x && a.constructor === x && a.g === z
        ? a.h
        : 'type_error:Const'
    );
  }
  var B = {};
  function D(a) {
    if (void 0 === w) {
      var b = null;
      var d = g.trustedTypes;
      if (d && d.createPolicy) {
        try {
          b = d.createPolicy('uf-api#html', {
            createHTML: u,
            createScript: u,
            createScriptURL: u,
          });
        } catch (k) {
          g.console && g.console.error(k.message);
        }
        w = b;
      } else w = b;
    }
    a = (b = w) ? b.createScriptURL(a) : a;
    return new A(a, B);
  }
  function E(a, b) {
    a.src =
      b instanceof A && b.constructor === A
        ? b.g
        : 'type_error:TrustedResourceUrl';
    (b = h(a.ownerDocument && a.ownerDocument.defaultView)) &&
      a.setAttribute('nonce', b);
  }
  try {
    new self.OffscreenCanvas(0, 0).getContext('2d');
  } catch (a) {}
  function F(a) {
    this.g = a || g.document || document;
  }
  function G(a) {
    a = a.g;
    var b = 'SCRIPT';
    'application/xhtml+xml' === a.contentType && (b = b.toLowerCase());
    return a.createElement(b);
  }
  function I(a, b, d) {
    a.timeOfStartCall = new Date().getTime();
    var k = d || g,
      c = k.document,
      n = a.nonce || h(k);
    n && !a.nonce && (a.nonce = n);
    if ('help' == a.flow) {
      var e = r(k);
      !a.helpCenterContext && e && (a.helpCenterContext = e.substring(0, 1200));
      e = !0;
      if (b && JSON && JSON.stringify) {
        var l = JSON.stringify(b);
        (e = 1200 >= l.length) && (a.psdJson = l);
      }
      e || (b = { invalidPsd: !0 });
    }
    e = [a, b, d];
    k.GOOGLE_FEEDBACK_START_ARGUMENTS = e;
    l = a.serverUri || '//www.google.com/tools/feedback';
    var H = k.GOOGLE_FEEDBACK_START;
    if (a.forceNewHelpPanel) {
      var f = G(
        c
          ? new F(9 == c.nodeType ? c : c.ownerDocument || c.document)
          : v || (v = new F())
      );
      n && f.setAttribute('nonce', n);
      f.onload = function () {
        k.startHelpPanel({
          helpcenter: a.helpCenterPath.split('/')[1],
          apiKey: 'testpage',
          locale: a.locale,
          enableSendFeedback: a.enableSendFeedback || !1,
          helpApiData: { helpApiConfig: a, productData: b, productWindow: d },
        });
      };
      E(f, C());
      c.body.appendChild(f);
    } else if (H) H.apply(k, e);
    else {
      e = l + '/load.js?';
      for (f in a)
        (l = a[f]),
          null == l ||
            t(l) ||
            (e += encodeURIComponent(f) + '=' + encodeURIComponent(l) + '&');
      f = G(
        c
          ? new F(9 == c.nodeType ? c : c.ownerDocument || c.document)
          : v || (v = new F())
      );
      n && f.setAttribute('nonce', n);
      E(f, D(e));
      c.body.appendChild(f);
    }
  }
  var J = ['userfeedback', 'api', 'startFeedback'],
    K = g;
  J[0] in K ||
    'undefined' == typeof K.execScript ||
    K.execScript('var ' + J[0]);
  for (var L; J.length && (L = J.shift()); )
    J.length || void 0 === I
      ? K[L] && K[L] !== Object.prototype[L]
        ? (K = K[L])
        : (K = K[L] = {})
      : (K[L] = I);
}.call(this));
